<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/branches','BranchController@Index')->name('branches.index');
//Route::get('/branches/create','BranchController@create')->name('branches.create');
//Route::get('/branches/edit/{id}','BranchController@edit')->name('branches.edit');
//Route::get('/branches/delete/{id}','BranchController@delete')->name('branches.delete');

//Route::post('/branches/store','BranchController@store');
//Route::patch('/branches/update/{id}','BranchController@update');

Route::get('/login','loginController@index')->name('login');
Route::post('/login-new','loginController@store')->name('pasok');
Route::get('/logout','loginController@logout')->name('gawas');

Route::resource('/branches','BranchController');
Route::get('/branches/delete/{id}','BranchController@delete')->name('branches.delete');

Route::resource('/employees','EmployeeController');
Route::get('/employees/delete/{id}','EmployeeController@delete')->name('employees.delete');

Route::resource('/customers','CustomerController');
Route::get('/customers/delete/{id}','CustomerController@delete')->name('customers.delete');

Route::resource('/users','UserController');
Route::get('/users/delete/{id}','UserController@delete')->name('users.delete');