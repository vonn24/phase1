@extends('layouts.app')
@section('content')
<h1 class="page-title">Users Module</h1>
@include('alert')
<div class="row">
    <div class="col-12">
        <a href="#add" data-toggle="modal" class="btn btn-primary mb-1">Create Entry</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Photo</th>
                    <th class="text-center">Username</th>
                    <th class="text-center">Email Address</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $data)
                <tr>
                    <td class="text-center">{{ $data->id}}</td>
                    <td class="text-center">
                      @if($data->photo != NULL)
                      <img class="profile-user-img img-fluid img-circle" src="<?php echo asset('public/images') ?>/{{ $data->photo }}" style="width:60px !important;">
                      @else
                      <img class="profile-user-img img-fluid img-circle" src="<?php echo asset('public/images/default.png') ?>" style="width:60px !important;">
                      @endif
                    </td>
                    <td class="text-center">{{ $data->name}}</td>
                    <td class="text-center">{{ $data->email}}</td>
                    <td class="text-center">
                        <a href="users.edit" class="btn btn-success">Edit</a>  
                        <a href="user.delete" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $users->links() !!}
    </div>
</div>
@endsection
@section('modals')
{!! Form::open(['method'=>'POST','action'=>'UserController@store','novalidate' => 'novalidate','files' => 'true']) !!}
  <div class="modal fade" id="add" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New Users</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
             <div class="row">
              <div class="col-md-12">
                <label for="exampleInputFile">Upload Photo</label>
                  <div class="form-group">
                    <div class="cutom-file">
                         <input type="file" class="custom-file-input" id="exampleInputFile" name="photo">
                         <label class="custom-file-label" for="exampleInputFile">Choose File</label>
                    </div>
                 </div>
                    <div class="form-group">
                        {!! Form::label('Enter Username') !!}
                        {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Enter Email') !!}
                        {!! Form::text('email',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Enter Password') !!}
                        {!! Form::password('password',['class'=>'form-control']) !!}
                    </div>
                 </div>
             </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  {!! Form::close() !!}
@endsection