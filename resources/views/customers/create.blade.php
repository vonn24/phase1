@extends('layouts.app')
@section('content')
<h1 class="page-title mt-2">Customer Module - Create</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('customers.index') }}" class="btn btn-primary mb-1">Back to Index</a>
        {!! Form::open(['method'=>'POST','action'=>'CustomerController@store']) !!}
        @include('alert')
        <div class="card mt-3">
            <div class="card-header">Add Customer</div>
            <div class="card-body">
                <div class="mb-3">
                    {!! Form::label('Enter Name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="mb-3">
                    {!! Form::label('Enter Name') !!}
                    {!! Form::text('name',null,['class'=>'form-control']) !!}
                </div>
                <div class="mb-3">
                    {!! Form::label('Enter Address') !!}
                    {!! Form::text('address',null,['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="card-footer">
                {!! Form::submit('Save Entry',['class'=>'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>    
@endsection