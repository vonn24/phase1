@extends('layouts.app')
@section('content')
<h1 class="page-title">Branch Module</h1>
<div class="row">
    <div class="col-12">
        <a href="{{ route('branches.create') }}" class="btn btn-primary mb-1">Create Entry</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Code</th>
                    <th class="text-center">Position</th>
                    <th class="text-center">Branch Name</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($branches as $data)
                <tr>
                    <td class="text-center">{{ $data->id }}</td>
                    <td class="text-center">{{ $data->codes}}</td>
                    <td class="text-center">{{ $data->usertype }}</td>
                    <td class="text-center">{{ $data->name}}</td>
                    <td class="text-center">
                        <a href="{{ action('BranchController@edit',$data->id) }}" class="btn btn-success">Edit</a>  
                        <a href="{{ action('BranchController@delete',$data->id) }}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $branches->links() !!}
    </div>
</div>
@endsection