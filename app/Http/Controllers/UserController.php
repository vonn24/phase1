<?php

namespace App\Http\Controllers;

use App\User;
use Request;
use Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::paginate(10);
        return view('users.index',compact('users'));
    }

    public function store()
    {
        $validator = Validator::make(Request::all(), [
            'name'                  =>  'required|unique:users',
            'email'                 =>  'required|email|unique:users',
            'password'              =>  'required|min:6',
            'photo'                 =>  'required',
        ],
        [
            'name.required'         =>  'Username Required',
            'email.required'        =>  'Email Required',
            'password.required'     =>  'Password Required',
            'photo.required'        =>  'Photo Required',

        ]);


        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $file = Request::file('photo');
        $file_blob = Request::file('photo_blob');

        if($file != NULL){
            $extension = $file->getClientOriginalExtension();
            $fileName = str::random(50).'.'.$extension;
            $file->move(public_path().'/images',$fileName);
        }else{
            $fileName = 'default.png';
        }

        User::create([
            'name'                  =>    Request::get('name'),
            'email'                 =>    Request::get('email'),
            'password'              =>    \Hash::make(preg_replace('/\s*/', '',(Request::get('password')))),
            'photo'                 =>    $fileName,
        ]);
        toastr()->success('Data has been saved successfully!');
        return redirect()->back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit',compact('user'));
    }

    public function update()
    {
        $user = User::find($id);
        $validator = Validator::make(Request::all(), [
            'name'                 =>  "required|unique:users,name,$user->id,id",
            'email'                =>  "required|unique:users,email,$user->id,id",
        ],
        [
            'name.required'        =>  'User Name Required',
            'email.required'       =>  'User Email Required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $user->update(Request::all());
        toastr()->success('Data has been updated successfully!');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $empluseroyee = User::find($id);
        toastr()->success('Data has been deleted successfully!');
        $user->delete();
        return redirect()->route('users.index');
    }
}
