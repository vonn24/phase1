<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Employee;
use Request;
use Validator;
use Auth;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $branches = Branch::paginate(10);
        return view('branches.index',compact('branches'));
    }

    public function create()
    {
        $branches = Branch::get();
        return view('branches.create');

    }

    public function store()
    {
        $validator = Validator::make(Request::all(), [
            'codes'                =>  'required',
            'name'                 =>  'required',
        ],
        [
            'codes      .required' =>  'Select Code Required',
            'name.required'        =>  'Branch Name Required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        Branch::create(Request::all());
        toastr()->success('Data has been saved successfully!');
        return redirect()->back();
    }

    public function edit($id)
    {
        $branch = Branch::find($id);
        return view('branches.edit',compact('branch'));
    }
    
    public function update($id)
    {
        $branch = Branch::find($id);
        $validator = Validator::make(Request::all(), [
            'name'                 =>  'required',
            'codes'                =>  'required',
        ],
        [
            'name.required'      =>  'Branch Name Required',
            'codes.required'     =>  'Branch Code Required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $branch->update(Request::all());
        toastr()->success('Data has been updated successfully!');
        return redirect()->route('branches.index');
    }

    public function delete($id)
    {
        $branch = Branch::find($id);
        toastr()->success('Data has been deleted successfully!');
        $branch->delete();
        return redirect()->route('branches.index');
    }
}
