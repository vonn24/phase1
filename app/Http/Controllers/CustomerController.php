<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Employee;
use Request;
use Validator;
use Auth;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $employees = Employee::paginate(5);
        return view('employees.index',compact('employees'));
    }

    public function create()
    {
        $branches = Branch::pluck('name','id');
        return view('employees.create',compact('branches'));
    }

    public function store()
    {
        $validator = Validator::make(Request::all(), [
            'branch_id'            =>  'required',
            'name'                 =>  'required',
            'address'              =>  'required',
        ],
        [
            'branch_id.required'   =>  'Select Branch Required',
            'name.required'        =>  'Employee Name Required',
            'address.required'     =>  'Employee Address Required',

        ]);


        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        Employee::create(Request::all());
        toastr()->success('Employee has been saved successfully!');
        return redirect()->back();
    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('employees.edit',compact('employee'));
    }

    public function update($id)
    {
        $employee = Employee::find($id);
        $validator = Validator::make(Request::all(), [
            'name'                 =>  "required|unique:employees,name,$employee->id,id",
            'address'              =>  "required|unique:employees,address,$employee->id,id",
        ],
        [
            'name.required'        =>  'Employee Name Required',
            'address.required'     =>  'Employee Address Required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $employee->update(Request::all());
        toastr()->success('Data has been updated successfully!');
        return redirect()->route('employees.index');

    }

    public function delete($id)
    {
        $employee = Employee::find($id);
        toastr()->success('Data has been deleted successfully!');
        $employee->delete();
        return redirect()->route('employees.index');
    }
}
